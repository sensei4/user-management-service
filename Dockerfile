FROM maven:3.9.0-eclipse-temurin-17 AS build

COPY pom.xml /build/
COPY src /build/src/

WORKDIR /build/
RUN mvn clean package -DskipTests

FROM bellsoft/liberica-openjdk-alpine:17.0.5 AS run

WORKDIR /app
COPY --from=build /build/target/user.management.service-0.0.1.jar /app/

ENTRYPOINT ["java", "-jar", "user.management.service-0.0.1.jar"]