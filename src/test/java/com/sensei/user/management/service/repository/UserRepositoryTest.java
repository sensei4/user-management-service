package com.sensei.user.management.service.repository;

import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.model.UserType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static com.sensei.user.management.service.utils.TestConstants.USER1;
import static com.sensei.user.management.service.utils.TestConstants.MEMBERS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@Testcontainers
@DataMongoTest
@TestPropertySource(properties =
	"spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration")
public class UserRepositoryTest {

	@Container
	static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:6.0.5");

	@DynamicPropertySource
	static void setProperties(DynamicPropertyRegistry registry) {
		registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
	}

	@Autowired
	private UserRepository userRepository;

	@BeforeEach
	void seed() {
		userRepository.deleteAll();
		userRepository.saveAll(MEMBERS);
	}

	@AfterEach
	void cleanUp() {
		userRepository.deleteAll();
	}

	@Test
	public void shouldSaveNewUser() {
		long initialCount = userRepository.count();
		userRepository.save(new User("3",
			"New",
			"Doe",
			LocalDate.of(2000, 1, 1),
			"new@mail.com",
			"password",
			"07123456789",
			UserType.MEMBER,
			Collections.emptyList(),
			LocalDate.now()
		));
		long updatedCount = userRepository.count();
		assertEquals(initialCount + 1, updatedCount);
	}

	@Test
	public void shouldReturnAllUsers() {
		assertEquals(2, userRepository.findAll().size());
	}

	@Test
	public void shouldReturnUserByID() {
		final Optional<User> result = userRepository.findById("1");

		assertTrue(result.isPresent());
		assertEquals(USER1, result.get());
	}

	@Test
	public void shouldReturnUserByEmail() {
		final Optional<User> result = userRepository.findByEmailAddress("one@mail.com");

		assertTrue(result.isPresent());
		assertEquals(USER1, result.get());
	}

	@Test
	public void shouldUpdateUser() {
		// GIVEN
		// A user exists with id '1'
		final Optional<User> inserted = userRepository.findById("1");
		assertTrue(inserted.isPresent());
		assertEquals(LocalDate.of(2000,1, 1), inserted.get().getDateOfBirth());

		//WHEN
		// This user is updated
		final User updatedUser = new User("1",
			"One",
			"Doe",
			LocalDate.of(1999, 1, 1),
			"one@mail.com",
			"password",
			"07123456789",
			UserType.MEMBER,
			Collections.emptyList(),
			LocalDate.of(2022, 1, 1)
		);
		userRepository.save(updatedUser);

		//ASSERT
		// The updated value is persisted
		final Optional<User> result = userRepository.findById("1");
		assertTrue(result.isPresent());
		assertEquals(LocalDate.of(1999, 1, 1),
			result.get().getDateOfBirth());

	}

	@Test
	public void shouldDelete() {
		// GIVEN
		// A users exist
		assertEquals(2, userRepository.findAll().size());

		//WHEN
		// A user is deleted
		userRepository.deleteById("1");

		//ASSERT
		// The user is removed from the collection
		assertEquals(1, userRepository.findAll().size());
	}
}