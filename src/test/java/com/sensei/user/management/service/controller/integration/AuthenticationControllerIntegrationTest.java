package com.sensei.user.management.service.controller.integration;

import com.sensei.user.management.service.model.AuthenticationRequest;
import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.model.exception.InvalidTokenException;
import com.sensei.user.management.service.model.exception.PasswordNotAcceptedException;
import com.sensei.user.management.service.model.exception.UserEmailConflictException;
import com.sensei.user.management.service.model.exception.UserNotFoundException;
import com.sensei.user.management.service.service.AuthenticationService;
import com.sensei.user.management.service.service.TokenService;
import com.sensei.user.management.service.utils.TestRepositoryProvider;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.sensei.user.management.service.utils.TestConstants.AUTHENTICATION_RESPONSE;
import static com.sensei.user.management.service.utils.TestConstants.AUTH_HEADER_VALUE;
import static com.sensei.user.management.service.utils.TestConstants.TOKEN_VALUE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Import(TestRepositoryProvider.class)
public class AuthenticationControllerIntegrationTest {

	@MockBean
	private AuthenticationService authenticationService;

	@MockBean
	private TokenService tokenService;

	@Autowired
	MockMvc mockMvc;

	@Test
	public void canRegister() throws Exception {
		when(authenticationService.register(any(User.class)))
			.thenReturn(AUTHENTICATION_RESPONSE);

		mockMvc.perform(
			post("/api/v1/auth/register")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content("""
					{
					    "firstName":"One",
					    "lastName":"Doe",
					    "dateOfBirth":"2000-01-01",
					    "emailAddress": "one@mail.com",
					    "contactNumber":"07123456789",
					    "password": "password"
					}
					"""))
			.andDo(print())
			.andExpectAll(
				status().isCreated(),
				content().contentType(MediaType.APPLICATION_JSON),
				content().json("""
					{
						"access":"access",
						"refresh":"refresh"
					}
					""", true));
	}

	@Test
	public void registerReturns400WhenIdPresentInRequest() throws Exception {
		when(authenticationService.register(any(User.class)))
			.thenThrow(new IllegalArgumentException("Attribute 'id' not permitted"));

		mockMvc.perform(
			post("/api/v1/auth/register")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content("""
					{
						"id": "1",
					    "firstName":"One",
					    "lastName":"Doe",
					    "dateOfBirth":"2000-01-01",
					    "emailAddress": "one@mail.com",
					    "contactNumber":"07123456789",
					    "password": "password"
					}
					"""))
			.andDo(print())
			.andExpectAll(
				status().isBadRequest(),
				content().contentType(MediaType.APPLICATION_JSON),
				content().json("""
					{
						"error":"Attribute 'id' not permitted"
					}
					""", true));
	}

	@Test
	public void registerReturns409WhenEmailIsAlreadyRegistered() throws Exception {
		when(authenticationService.register(any(User.class)))
			.thenThrow(new UserEmailConflictException("Email address registered to an existing account"));

		mockMvc.perform(
			post("/api/v1/auth/register")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content("""
					{
					    "firstName":"One",
					    "lastName":"Doe",
					    "dateOfBirth":"2000-01-01",
					    "emailAddress": "one@mail.com",
					    "contactNumber":"07123456789",
					    "password": "password"
					}
					"""))
			.andDo(print())
			.andExpectAll(
				status().isConflict(),
				content().contentType(MediaType.APPLICATION_JSON),
				content().json("""
					{
						"error":"Email address registered to an existing account"
					}
					""", true));
	}

	@Test
	public void canAuthenticate() throws Exception {
		when(authenticationService.authenticate(any(AuthenticationRequest.class)))
			.thenReturn(AUTHENTICATION_RESPONSE);

		mockMvc.perform(
				post("/api/v1/auth/login")
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON)
					.content("""
					{
						"emailAddress": "one@mail.com",
						"password": "password"
					}
					"""))
			.andDo(print())
			.andExpectAll(
				status().isOk(),
				content().contentType(MediaType.APPLICATION_JSON),
				content().json("""
					{
						"access":"access",
						"refresh":"refresh"
					}
					""", true));
	}

	@Test
	public void authenticateReturns401OnPasswordNotAcceptedException() throws Exception {
		when(authenticationService.authenticate(any(AuthenticationRequest.class)))
			.thenThrow(new PasswordNotAcceptedException("Password did not match for one@mail.com"));

		mockMvc.perform(
				post("/api/v1/auth/login")
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON)
					.content("""
					{
						"emailAddress": "one@mail.com",
						"password": "password"
					}
					"""))
			.andDo(print())
			.andExpectAll(
				status().isUnauthorized(),
				content().contentType(MediaType.APPLICATION_JSON),
				content().json("""
					{
						"error": "Password did not match for one@mail.com"
					}
					""", true));
	}

	@Test
	public void loginReturns404OnUserNotFoundException() throws Exception {
		when(authenticationService.authenticate(any(AuthenticationRequest.class)))
			.thenThrow(new UserNotFoundException("User not found"));

		mockMvc.perform(
				post("/api/v1/auth/login")
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON)
					.content("""
					{
						"emailAddress": "one@mail.com",
						"password": "password"
					}
					"""))
			.andDo(print())
			.andExpectAll(
				status().isNotFound());
	}

	@Test
	public void canRefresh() throws Exception {
		when(authenticationService.refresh(TOKEN_VALUE))
			.thenReturn(AUTHENTICATION_RESPONSE);

		mockMvc.perform(
				get("/api/v1/auth/refresh")
					.accept(MediaType.APPLICATION_JSON)
					.header(AUTHORIZATION, AUTH_HEADER_VALUE))
			.andDo(print())
			.andExpectAll(
//				status().isOk(),
				content().contentType(MediaType.APPLICATION_JSON),
				content().json("""
					{
						"access":"access",
						"refresh":"refresh"
					}
					""", true));
	}

	@Test
	public void refreshReturns401OnMissingAuthHeaderException() throws Exception {
		mockMvc.perform(
				get("/api/v1/auth/refresh")
					.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpectAll(
				status().isUnauthorized(),
				content().contentType(MediaType.APPLICATION_JSON),
				content().json("""
					{
						"error": "Authorization header not presented correctly"
					}
					""", true));

		verifyNoInteractions(authenticationService);
	}

	@Test
	public void refreshReturns401OnInvalidTokenException() throws Exception {
		when(authenticationService.refresh(TOKEN_VALUE))
			.thenThrow(new InvalidTokenException("Invalid token presented"));

		mockMvc.perform(
				get("/api/v1/auth/refresh")
					.accept(MediaType.APPLICATION_JSON)
					.header(AUTHORIZATION, AUTH_HEADER_VALUE))
			.andDo(print())
			.andExpectAll(
				status().isUnauthorized(),
				content().contentType(MediaType.APPLICATION_JSON),
				content().json("""
					{
						"error": "Invalid token presented"
					}
					""", true));
	}
}
