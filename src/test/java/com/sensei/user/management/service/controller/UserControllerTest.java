package com.sensei.user.management.service.controller;

import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.sensei.user.management.service.utils.TestConstants.USER1;
import static com.sensei.user.management.service.utils.TestConstants.MEMBERS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

	@Mock
	private UserService service;

	@InjectMocks
	private UserController controller;

	@Test
	public void canGetAll() {
		when(service.getAll()).thenReturn(MEMBERS);

		List<User> result = controller.getAll();

		verify(service).getAll();
		assertEquals(MEMBERS, result);
	}

	@Test
	public void canGetByID() {
		when(service.getById("1")).thenReturn(USER1);

		User result = controller.getById("1");

		verify(service).getById("1");
		assertEquals(USER1, result);
	}

	@Test
	public void canUpdateUser() {
		controller.updateUser("1", USER1);

		verify(service).mergeAndUpdate("1", USER1);
	}

	@Test
	public void canDelete() {
		controller.deleteUser("3");

		verify(service).deleteUser("3");
	}
}
