package com.sensei.user.management.service.controller.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.model.exception.IllegalUserUpdateException;
import com.sensei.user.management.service.model.exception.UserNotFoundException;
import com.sensei.user.management.service.security.JwtAuthFilter;
import com.sensei.user.management.service.service.UserService;
import com.sensei.user.management.service.utils.TestRepositoryProvider;
import jakarta.servlet.ServletException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static com.sensei.user.management.service.utils.TestConstants.USER1;
import static com.sensei.user.management.service.utils.TestConstants.MEMBERS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@Import(TestRepositoryProvider.class)
public class UserControllerIntegrationTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	UserService service;

	@Autowired
	ObjectMapper objectMapper;

	@Test
	public void canGetAll() throws Exception {
		when(service.getAll()).thenReturn(MEMBERS);

		mockMvc.perform(get("/api/v1/users"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_VALUE))
			.andExpect(content().json("""
				[
				  {
				    "id": "1",
				    "firstName": "One",
				    "lastName": "Doe",
				    "dateOfBirth": "2000-01-01",
				    "emailAddress": "one@mail.com",
				    "contactNumber": "07123456789",
				    "userType": "MEMBER",
				    "emergencyContactList": [],
				    "created": "2022-01-01"
				  },
				  {
				    "id": "2",
				    "firstName": "Two",
				    "lastName": "Doe",
				    "dateOfBirth": "2000-01-01",
				    "emailAddress": "two@mail.com",
				    "contactNumber": "07123456789",
				    "userType": "MEMBER",
				    "emergencyContactList": [],
				    "created": "2022-01-01"
				  }
				]
				""", true));
	}

	@Test
	public void canGetByID() throws Exception {
		when(service.getById("1")).thenReturn(USER1);

		mockMvc.perform(get("/api/v1/users/1")
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().contentType(APPLICATION_JSON))
			.andExpect(content().json("""
				{
				  "id": "1",
				  "firstName": "One",
				  "lastName": "Doe",
				  "dateOfBirth": "2000-01-01",
				  "emailAddress": "one@mail.com",
				  "contactNumber": "07123456789",
				  "userType": "MEMBER",
				  "emergencyContactList": [],
				  "created": "2022-01-01"
				}
				""", true));
	}

	@Test
	public void getByIDReturns404OnUserNotFoundException() throws Exception {
		when(service.getById("1")).thenThrow(new UserNotFoundException("User not found"));

		mockMvc.perform(get("/api/v1/users/1")
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isNotFound());
	}

	@Test
	public void canUpdate() throws Exception {
		final String content = objectMapper.writeValueAsString(USER1);
		mockMvc.perform(put("/api/v1/users/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().isNoContent());

		verify(service).mergeAndUpdate(eq("1"), any(User.class));
	}

	@Test
	public void updateReturns400OnIllegalUserUpdateException() throws Exception {
		final String content = objectMapper.writeValueAsString(USER1);
		doThrow(new IllegalUserUpdateException("ID must match in path and body"))
			.when(service).mergeAndUpdate(eq("2"), any());

		mockMvc.perform(put("/api/v1/users/2")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().isBadRequest())
			.andExpect(content().json("""
				{
					"error":"ID must match in path and body"
				}
				""", true));
	}

	@Test
	public void canDelete() throws Exception {
		mockMvc.perform(delete("/api/v1/users/1"))
			.andDo(print())
			.andExpect(status().isNoContent());

		verify(service).deleteUser("1");
	}
}
