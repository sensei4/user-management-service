package com.sensei.user.management.service.utils;

import com.sensei.user.management.service.model.AuthenticationRequest;
import com.sensei.user.management.service.model.AuthenticationResponse;
import com.sensei.user.management.service.model.Token;
import com.sensei.user.management.service.model.TokenType;
import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.model.UserType;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class TestConstants {

	public static final User NEW_USER = new User(null,
		"New",
		"User",
		LocalDate.of(2000, 1, 1),
		"newuser@mail.com",
		"password",
		"07123456789",
		UserType.MEMBER,
		Collections.emptyList(),
		LocalDate.of(2022, 1, 1)
	);

	public static final User USER1 = new User("1",
		"One",
		"Doe",
		LocalDate.of(2000, 1, 1),
		"one@mail.com",
		"password",
		"07123456789",
		UserType.MEMBER,
		Collections.emptyList(),
		LocalDate.of(2022, 1, 1)
	);
	public static final User USER2 = new User(
		"2",
		"Two",
		"Doe",
		LocalDate.of(2000, 1, 1),
		"two@mail.com",
		"password",
		"07123456789",
		UserType.MEMBER,
		Collections.emptyList(),
		LocalDate.of(2022, 1, 1)
	);
	public static final User ADMIN_USER = new User(
		"3",
		"Admin",
		"User",
		LocalDate.of(2000, 1, 1),
		"admin@mail.com",
		"password",
		"07123456789",
		UserType.ADMIN,
		Collections.emptyList(),
		LocalDate.of(2022, 1, 1)
	);

	public static final List<User> ALL_USERS = List.of(USER1, USER2, ADMIN_USER);
	public static final List<User> MEMBERS = List.of(USER1, USER2);

	public static final AuthenticationRequest AUTHENTICATION_REQUEST = new AuthenticationRequest("one@mail.com",
		"password");

	public static final AuthenticationResponse AUTHENTICATION_RESPONSE = new AuthenticationResponse("access", "refresh");

	public static final Token ACCESS_TOKEN = new Token("123", TokenType.ACCESS, "xyz","user1");
	public static final Token REFRESH_TOKEN = new Token("345", TokenType.REFRESH, "xyz","user1");

	public static final List<Token> TOKENS = List.of(ACCESS_TOKEN, REFRESH_TOKEN);

	public static final String SECRET_KEY = "244226452948404D635166546A576E5A7234753778214125442A462D4A614E64";

	public static final String TOKEN_VALUE = "abc.def.xyz";
	public static final String AUTH_HEADER_VALUE = "Bearer " + TOKEN_VALUE;
}
