package com.sensei.user.management.service.service;

import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.model.UserType;
import com.sensei.user.management.service.model.exception.IllegalUserUpdateException;
import com.sensei.user.management.service.model.exception.UserNotFoundException;
import com.sensei.user.management.service.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.sensei.user.management.service.utils.TestConstants.ALL_USERS;
import static com.sensei.user.management.service.utils.TestConstants.NEW_USER;
import static com.sensei.user.management.service.utils.TestConstants.USER1;
import static com.sensei.user.management.service.utils.TestConstants.USER2;
import static com.sensei.user.management.service.utils.TestConstants.MEMBERS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

	@Mock
	private UserRepository repository;

	@InjectMocks
	private UserService service;

	@Test
	public void canSave() {
		when(repository.save(NEW_USER)).thenReturn(USER1);

		User result = service.save(NEW_USER);

		assertEquals(USER1, result);
		verify(repository).save(NEW_USER);
	}

	@Test
	public void canGetAllUsers() {
		when(repository.findAll()).thenReturn(MEMBERS);

		List<User> result = service.getAll();

		assertEquals(MEMBERS, result);
		verify(repository).findAll();
	}

	@Test
	public void canGetUserByID() {
		when(repository.findById("1")).thenReturn(Optional.of(USER1));

		User result = service.getById("1");

		assertEquals(USER1, result);
	}

	@Test
	public void getByIDThrowsUserNotFoundExceptionWhenNotFound() {
		when(repository.findById("1")).thenReturn(Optional.empty());

		assertThrows(UserNotFoundException.class,
			() -> service.getById("1"));

		verify(repository).findById("1");
	}

	@Test
	public void canGetUserByEmail() {
		when(repository.findByEmailAddress("one@mail.com")).thenReturn(Optional.of(USER1));

		User result = service.getByEmailAddress("one@mail.com");

		assertEquals(USER1, result);
		verify(repository).findByEmailAddress("one@mail.com");
	}

	@Test
	public void getByEmailThrowsUserNotFoundExceptionWhenNotFound() {
		when(repository.findByEmailAddress("one@mail.com")).thenReturn(Optional.empty());

		assertThrows(UserNotFoundException.class,
			() -> service.getByEmailAddress("one@mail.com"));
		verify(repository).findByEmailAddress("one@mail.com");
	}

	@Test
	public void canMergeAndUpdate() {
		when(repository.findById("1")).thenReturn(Optional.of(USER1));
		final User updatedUser = new User("1",
			"One",
			"Updated",
			LocalDate.of(2000, 1, 1),
			"one@mail.com",
			"password",
			"07123456789",
			UserType.MEMBER,
			Collections.emptyList(),
			LocalDate.now()
		);

		service.mergeAndUpdate("1", updatedUser);

		verify(repository).save(updatedUser);
	}

	@Test
	public void mergeAndUpdateThrowsExceptionWhenIDsDoNotMatch() {
		assertThrows(IllegalUserUpdateException.class,
			() -> service.mergeAndUpdate("1", USER2));

		verifyNoInteractions(repository);
	}

	@Test
	public void canDelete() {
		service.deleteUser("1");

		verify(repository).deleteById("1");
	}

	@Test
	public void canCheckAdminExistsFalse() {
		when(repository.findAll()).thenReturn(MEMBERS);

		boolean result = service.adminExists();

		assertFalse(result);
		verify(repository).findAll();
	}

	@Test
	public void canCheckAdminExistsTrue() {
		when(repository.findAll()).thenReturn(ALL_USERS);

		boolean result = service.adminExists();

		assert(result);
		verify(repository).findAll();
	}
}
