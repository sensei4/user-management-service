package com.sensei.user.management.service.utils;

import com.sensei.user.management.service.repository.TokenRepository;
import com.sensei.user.management.service.repository.UserRepository;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;

@TestConfiguration
public class TestRepositoryProvider {

	@MockBean
	UserRepository userRepository;

	@MockBean
	TokenRepository tokenRepository;
}
