package com.sensei.user.management.service.controller;

import com.sensei.user.management.service.model.AuthenticationResponse;
import com.sensei.user.management.service.model.exception.PasswordNotAcceptedException;
import com.sensei.user.management.service.service.AuthenticationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;

import static com.sensei.user.management.service.utils.TestConstants.AUTHENTICATION_REQUEST;
import static com.sensei.user.management.service.utils.TestConstants.AUTHENTICATION_RESPONSE;
import static com.sensei.user.management.service.utils.TestConstants.USER1;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthenticationControllerTest {

	@Mock
	private AuthenticationService service;

	@InjectMocks
	private AuthenticationController controller;

	@Test
	public void canRegister() {
		when(service.register(USER1)).thenReturn(AUTHENTICATION_RESPONSE);

		AuthenticationResponse result = controller.register(USER1);

		assertEquals(AUTHENTICATION_RESPONSE, result);
	}

	@Test
	public void canAuthenticate() throws PasswordNotAcceptedException  {
		when(service.authenticate(AUTHENTICATION_REQUEST))
			.thenReturn(AUTHENTICATION_RESPONSE);

		AuthenticationResponse result = controller.authenticate(AUTHENTICATION_REQUEST);

		assertEquals(AUTHENTICATION_RESPONSE, result);
	}

	@Test
	public void canRefresh() {
		final MockHttpServletRequest request =  new MockHttpServletRequest();
		request.addHeader("Authorization", "Bearer abc.def.xyz");
		when(service.refresh("abc.def.xyz")).thenReturn(AUTHENTICATION_RESPONSE);

		AuthenticationResponse result = controller.refresh(request);

		assertEquals(AUTHENTICATION_RESPONSE, result);
	}
}
