package com.sensei.user.management.service.service;

import com.sensei.user.management.service.model.AuthenticationResponse;
import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.model.UserType;
import com.sensei.user.management.service.model.exception.InvalidTokenException;
import com.sensei.user.management.service.model.exception.PasswordNotAcceptedException;
import com.sensei.user.management.service.model.exception.UserEmailConflictException;
import com.sensei.user.management.service.model.exception.UserNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.sensei.user.management.service.utils.TestConstants.ACCESS_TOKEN;
import static com.sensei.user.management.service.utils.TestConstants.AUTHENTICATION_REQUEST;
import static com.sensei.user.management.service.utils.TestConstants.NEW_USER;
import static com.sensei.user.management.service.utils.TestConstants.REFRESH_TOKEN;
import static com.sensei.user.management.service.utils.TestConstants.USER1;
import static com.sensei.user.management.service.utils.TestConstants.USER2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthenticationServiceTest {

	@Mock
	UserService userService;

	@Mock
	TokenService tokenService;

	@Mock
	BCryptPasswordEncoder passwordEncoder;

	@InjectMocks
	AuthenticationService service;

	@Captor
	ArgumentCaptor<User> userArgumentCaptor;

	@Test
	public void canRegister() {
		when(userService.getByEmailAddress(NEW_USER.getEmailAddress())).
			thenThrow(new UserNotFoundException("User not found"));
		when(passwordEncoder.encode(NEW_USER.getPassword())).thenReturn("xyz");
		when(userService.adminExists()).thenReturn(true);
		when(userService.save(NEW_USER)).thenReturn(USER1);
		when(tokenService.generateAccessToken(NEW_USER)).thenReturn(ACCESS_TOKEN);
		when(tokenService.generateRefreshToken(NEW_USER)).thenReturn(REFRESH_TOKEN);

		AuthenticationResponse result = service.register(NEW_USER);

		verify(userService).save(userArgumentCaptor.capture());
		verify(tokenService).generateAccessToken(NEW_USER);
		verify(tokenService).generateRefreshToken(NEW_USER);
		assertEquals(new AuthenticationResponse(ACCESS_TOKEN.getValue(), REFRESH_TOKEN.getValue()), result);
		assertEquals(UserType.MEMBER, userArgumentCaptor.getValue().getUserType());
	}

	@Test
	public void registerSetsFirstUserAsAdmin() {
		when(userService.getByEmailAddress(NEW_USER.getEmailAddress())).
			thenThrow(new UserNotFoundException("User not found"));
		when(passwordEncoder.encode(NEW_USER.getPassword())).thenReturn("xyz");
		when(userService.adminExists()).thenReturn(false);
		when(userService.save(NEW_USER)).thenReturn(USER1);
		when(tokenService.generateAccessToken(NEW_USER)).thenReturn(ACCESS_TOKEN);
		when(tokenService.generateRefreshToken(NEW_USER)).thenReturn(REFRESH_TOKEN);

		AuthenticationResponse result = service.register(NEW_USER);

		verify(userService).save(userArgumentCaptor.capture());
		verify(tokenService).generateAccessToken(NEW_USER);
		verify(tokenService).generateRefreshToken(NEW_USER);
		assertEquals(new AuthenticationResponse(ACCESS_TOKEN.getValue(), REFRESH_TOKEN.getValue()), result);
		assertEquals(UserType.ADMIN, userArgumentCaptor.getValue().getUserType());
	}

	@Test
	public void registerThrowsIllegalArgumentExceptionWhenIdPresent() {
		assertThrows(IllegalArgumentException.class,
			() -> service.register(USER1));

		verifyNoInteractions(userService);
		verifyNoInteractions(tokenService);
	}

	@Test
	public void registerThrowsUserEmailConflictExceptionWhenEmailAlreadyRegistered() {
		when(userService.getByEmailAddress(NEW_USER.getEmailAddress())).thenReturn(USER2);

		assertThrows(UserEmailConflictException.class,
			() -> service.register(NEW_USER));

		verifyNoMoreInteractions(userService);
		verifyNoInteractions(tokenService);
	}

	@Test
	public void canAuthenticate() {
		when(userService.getByEmailAddress(AUTHENTICATION_REQUEST.getEmail()))
			.thenReturn(USER1);
		when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
		when(tokenService.generateAccessToken(USER1)).thenReturn(ACCESS_TOKEN);
		when(tokenService.generateRefreshToken(USER1)).thenReturn(REFRESH_TOKEN);

		AuthenticationResponse result = service.authenticate(AUTHENTICATION_REQUEST);

		assertEquals(new AuthenticationResponse(ACCESS_TOKEN.getValue(), REFRESH_TOKEN.getValue()), result);
		verify(passwordEncoder).matches(AUTHENTICATION_REQUEST.getPassword(), USER1.getPassword());
		verify(tokenService).generateAccessToken(USER1);
		verify(tokenService).generateRefreshToken(USER1);
	}

	@Test
	public void authenticateThrowsPasswordNotAcceptedException() {
		when(userService.getByEmailAddress(AUTHENTICATION_REQUEST.getEmail()))
			.thenReturn(USER1);
		when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);

		assertThrows(PasswordNotAcceptedException.class,
			()-> service.authenticate(AUTHENTICATION_REQUEST));
	}

	@Test
	public void canRefresh() {
		when(tokenService.isValidRefreshToken("abc.def.xyz")).thenReturn(true);
		when(tokenService.getSubject("abc.def.xyz")).thenReturn("1");
		when(userService.getById("1")).thenReturn(USER1);
		when(tokenService.generateAccessToken(USER1)).thenReturn(ACCESS_TOKEN);
		when(tokenService.generateRefreshToken(USER1)).thenReturn(REFRESH_TOKEN);

		AuthenticationResponse result = service.refresh("abc.def.xyz");
		assertEquals(new AuthenticationResponse(ACCESS_TOKEN.getValue(), REFRESH_TOKEN.getValue()), result);
	}

	@Test
	public void refreshInvalidTokenException() {
		when(tokenService.isValidRefreshToken("abc.def.xyz")).thenReturn(false);

		assertThrows(InvalidTokenException.class,
			() -> service.refresh("abc.def.xyz"));

		verify(tokenService).isValidRefreshToken("abc.def.xyz");
		verifyNoMoreInteractions(tokenService);
		verifyNoInteractions(userService);
	}
}
