package com.sensei.user.management.service.service;

import com.sensei.user.management.service.model.Token;
import com.sensei.user.management.service.model.TokenType;
import com.sensei.user.management.service.repository.TokenRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Clock;
import java.util.Date;
import java.util.Optional;

import static com.sensei.user.management.service.utils.TestConstants.ACCESS_TOKEN;
import static com.sensei.user.management.service.utils.TestConstants.REFRESH_TOKEN;
import static com.sensei.user.management.service.utils.TestConstants.SECRET_KEY;
import static com.sensei.user.management.service.utils.TestConstants.USER1;
import static com.sensei.user.management.service.utils.TestConstants.USER2;
import static java.util.concurrent.TimeUnit.MINUTES;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TokenServiceTest {

	@Mock
	Clock clock;

	@Mock
	io.jsonwebtoken.Clock jwtClock;

	@Mock
	TokenRepository repository;

	@InjectMocks
	TokenService service;

	@Captor
	ArgumentCaptor<Token> captor;

	@BeforeEach
	public void setup() {
		ReflectionTestUtils.setField(service,
			"sharedSecret",
			SECRET_KEY);
	}

	@Test
	public void canGenerateAccessToken() {
		// Mocked 'clock' will always return epoch as current date
		// setting 'jwtClock' to match
		when(jwtClock.now()).thenReturn(new Date(0));

		service.generateAccessToken(USER1);

		verify(repository).save(captor.capture());

		final Token generatedToken = captor.getValue();
		assertEquals(USER1.getId(), generatedToken.getUserId());
		assertEquals(TokenType.ACCESS, generatedToken.getType());
		Jwts.parserBuilder()
			.setSigningKey(Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET_KEY)))
			.setClock(jwtClock)
			.requireIssuedAt(new Date(0)) // epoch
			.requireExpiration(new Date(600000L)) // epoch + 10 mins
			.requireSubject(USER1.getId())
			.requireIssuer("USER MANAGEMENT SERVICE")
			.build()
			.parseClaimsJws(generatedToken.getValue())
			.getBody();

	}

	@Test
	public void canGenerateRefreshToken() {
		when(jwtClock.now()).thenReturn(new Date(0));

		service.generateRefreshToken(USER2);

		verify(repository).save(captor.capture());

		final Token generatedToken = captor.getValue();
		assertEquals(USER2.getId(), generatedToken.getUserId());
		assertEquals(TokenType.REFRESH, generatedToken.getType());
		Jwts.parserBuilder()
			.setSigningKey(Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET_KEY)))
			.setClock(jwtClock)
			.requireIssuedAt(new Date(0)) // epoch
			.requireExpiration(new Date(720000L)) // epoch + 11 mins
			.requireSubject(USER2.getId())
			.requireIssuer("USER MANAGEMENT SERVICE")
			.build()
			.parseClaimsJws(generatedToken.getValue())
			.getBody();
	}

	@Test
	public void canDeleteTokensByUserId() {
		service.deleteTokensForUserId("1");

		verify(repository).deleteByUserId("1");
	}

	@Test
	public void canValidateRefreshToken() {
		String validToken = Jwts.builder()
			.setSubject("1")
			.setExpiration(new Date(System.currentTimeMillis() + MINUTES.toMillis(3)))
			.signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET_KEY)), SignatureAlgorithm.HS256)
			.compact();
		when(repository.findByValue(validToken)).thenReturn(Optional.of(REFRESH_TOKEN));

		assert(service.isValidRefreshToken(validToken));
	}
	@Test
	public void canValidateRefreshToken_expired() {
		String invalidToken = Jwts.builder()
			.setSubject("1")
			.setExpiration(new Date(System.currentTimeMillis() - MINUTES.toMillis(3)))
			.signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET_KEY)), SignatureAlgorithm.HS256)
			.compact();

		assertFalse(service.isValidRefreshToken(invalidToken));
		verifyNoInteractions(repository);
	}

	@Test
	public void canValidateRefreshToken_wrongType() {
		String validToken = Jwts.builder()
			.setSubject("1")
			.setExpiration(new Date(System.currentTimeMillis() + MINUTES.toMillis(3)))
			.signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET_KEY)), SignatureAlgorithm.HS256)
			.compact();
		when(repository.findByValue(validToken)).thenReturn(Optional.of(ACCESS_TOKEN));

		assertFalse(service.isValidRefreshToken(validToken));
	}

	@Test
	public void canValidateRefreshToken_notFound() {
		String validToken = Jwts.builder()
			.setSubject("1")
			.setExpiration(new Date(System.currentTimeMillis() + MINUTES.toMillis(3)))
			.signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET_KEY)), SignatureAlgorithm.HS256)
			.compact();
		when(repository.findByValue(validToken)).thenReturn(Optional.empty());

		assertFalse(service.isValidRefreshToken(validToken));
	}

	@Test void canGetSubject() {
		String validToken = Jwts.builder()
			.setSubject("XYZ123")
			.setExpiration(new Date(System.currentTimeMillis() + MINUTES.toMillis(3)))
			.signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET_KEY)), SignatureAlgorithm.HS256)
			.compact();

		assertEquals("XYZ123", service.getSubject(validToken));

	}
}
