package com.sensei.user.management.service;

import com.sensei.user.management.service.utils.TestRepositoryProvider;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

@SpringBootTest
@Import(TestRepositoryProvider.class)
class UserManagementServiceApplicationTests {

	// MongoDB autoconfiguration classes were excluded in src/test/resources/application.yml
	// and UserRepository mocked in TestRepositoryProvider, so that auto-wiring of other components
	// can be tested without dependency on a running database.
	// (UserRepositoryTest covers relevant autoconfiguration classes)

	@Test
	void contextLoads() {
	}

}
