package com.sensei.user.management.service.repository;

import com.sensei.user.management.service.model.Token;
import com.sensei.user.management.service.model.TokenType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;
import java.util.Optional;

import static com.sensei.user.management.service.utils.TestConstants.ACCESS_TOKEN;
import static com.sensei.user.management.service.utils.TestConstants.TOKENS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@Testcontainers
@DataMongoTest
@TestPropertySource(properties =
	"spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration")
public class TokenRepositoryTest {

	@Container
	static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:6.0.5");

	@DynamicPropertySource
	static void setProperties(DynamicPropertyRegistry registry) {
		registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
	}

	@Autowired
	private TokenRepository tokenRepository;

	@BeforeEach
	void seed() {
		tokenRepository.deleteAll();
		assertEquals(0, tokenRepository.count());
	}

	@Test
	public void canSaveNewTokenAndIdIsGenerated() {
		Token token = new Token(null, TokenType.ACCESS, "value","user1");

		assertNull(token.getId());
		tokenRepository.save(token);
		assertNotNull(token.getId());
		assertEquals(1, tokenRepository.count());
	}

	@Test
	public void canFindAll() {
		tokenRepository.saveAll(TOKENS);

		List<Token> result = tokenRepository.findAll();

		assertEquals(2, result.size());
		assertEquals(TOKENS, result);
	}

	@Test
	public void canFindById() {
		tokenRepository.save(ACCESS_TOKEN);

		Optional<Token> result = tokenRepository.findById("123");

		assert(result.isPresent());
		assertEquals(ACCESS_TOKEN, result.get());
	}

	@Test
	public void canFindByValue() {
		tokenRepository.save(ACCESS_TOKEN);

		Optional<Token> result = tokenRepository.findByValue("xyz");

		assert(result.isPresent());
		assertEquals(ACCESS_TOKEN, result.get());
	}

	@Test
	public void canDeleteById() {
		tokenRepository.save(ACCESS_TOKEN);
		assertEquals(1, tokenRepository.count());

		tokenRepository.deleteById("123");
		assertEquals(0, tokenRepository.count());
	}

	@Test
	public void canDeleteByUserId() {
		tokenRepository.save(ACCESS_TOKEN);
		assertEquals(1, tokenRepository.count());

		tokenRepository.deleteByUserId("user1");
		assertEquals(0, tokenRepository.count());
	}
}
