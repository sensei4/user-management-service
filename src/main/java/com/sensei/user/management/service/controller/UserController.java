package com.sensei.user.management.service.controller;

import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("api/v1/users")
public class UserController {
	@Autowired
	private UserService userService;

	@GetMapping(produces = APPLICATION_JSON_VALUE)
	public List<User> getAll() {
		return userService.getAll();
	}

	@GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
	public User getById(@PathVariable String id) {
		return userService.getById(id);
	}

	@PutMapping(value = "/{id}",
		consumes = APPLICATION_JSON_VALUE,
		produces = APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateUser(@PathVariable String id, @RequestBody User updatedUser) {
			userService.mergeAndUpdate(id, updatedUser);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteUser(@PathVariable String id) {
		userService.deleteUser(id);
	}
}