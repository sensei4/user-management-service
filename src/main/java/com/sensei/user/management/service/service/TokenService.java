package com.sensei.user.management.service.service;

import com.sensei.user.management.service.model.Token;
import com.sensei.user.management.service.model.TokenType;
import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.repository.TokenRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Clock;
import java.util.Date;

import static java.util.concurrent.TimeUnit.MINUTES;

@Service
@RequiredArgsConstructor
public class TokenService {

	private static final Long ACCESS_TOKEN_EXPIRATION = MINUTES.toMillis(10);
	private static final Long REFRESH_TOKEN_EXPIRATION = ACCESS_TOKEN_EXPIRATION + MINUTES.toMillis(2);

	private final Clock clock;
	private final TokenRepository repository;

	@Value("${application.security.jwt.shared-secret}")
	private String sharedSecret;

	public Token generateAccessToken(User user) {
		final String newAccessTokenValue = generateToken(user, ACCESS_TOKEN_EXPIRATION);
		final Token token = new Token(null,TokenType.ACCESS, newAccessTokenValue, user.getId());

		return repository.save(token);
	}

	public Token generateRefreshToken(User user) {
		final String newRefreshTokenValue = generateToken(user, REFRESH_TOKEN_EXPIRATION);
		final Token token = new Token(null, TokenType.REFRESH, newRefreshTokenValue, user.getId());

		return repository.save(token);
	}

	public void deleteTokensForUserId(String userId) {
		repository.deleteByUserId(userId);
	}

	public String getSubject(String token) {
		return extractAllClaims(token).getSubject();
	}

	public boolean isValidRefreshToken(String tokenValue) {
		try {
			return !isExpired(tokenValue) &&
				repository.findByValue(tokenValue)
					.map(token -> token.getType().equals(TokenType.REFRESH))
					.orElse(false);
		} catch (JwtException exception) {
			return false;
		}
	}

	private boolean isExpired(String tokenValue) {
		return extractAllClaims(tokenValue)
			.getExpiration()
			.before(new Date());
	}

	private Claims extractAllClaims(String token) {
		return Jwts
			.parserBuilder()
			.setSigningKey(getSigningKey())
			.build()
			.parseClaimsJws(token)
			.getBody();
	}

	private String generateToken(User user, Long duration) {
		long currentTimeInMillis = clock.millis();
		return Jwts.builder()
			.setSubject(user.getId())
			.setIssuer("USER MANAGEMENT SERVICE")
			.setIssuedAt(new Date(currentTimeInMillis))
			.setExpiration(new Date(currentTimeInMillis + duration))
			.signWith(getSigningKey(), SignatureAlgorithm.HS256)
			.compact();
	}

	private Key getSigningKey() {
		byte[] keyBytes = Decoders.BASE64.decode(sharedSecret);
		return Keys.hmacShaKeyFor(keyBytes);
	}
}
