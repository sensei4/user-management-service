package com.sensei.user.management.service.model;

public enum TokenType {
	ACCESS,
	REFRESH
}
