package com.sensei.user.management.service.controller;

import com.sensei.user.management.service.model.AuthenticationRequest;
import com.sensei.user.management.service.model.AuthenticationResponse;
import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.model.exception.MissingAuthHeaderException;
import com.sensei.user.management.service.model.exception.PasswordNotAcceptedException;
import com.sensei.user.management.service.service.AuthenticationService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("api/v1/auth")
public class AuthenticationController {
	@Autowired
	AuthenticationService authenticationService;

	@PostMapping(value = "/register",
		consumes = APPLICATION_JSON_VALUE,
		produces = APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public AuthenticationResponse register(@RequestBody User user) {
		return authenticationService.register(user);
	}

	@PostMapping(value = "/login",
		consumes = APPLICATION_JSON_VALUE,
		produces = APPLICATION_JSON_VALUE)
	public AuthenticationResponse authenticate(@RequestBody @NonNull AuthenticationRequest authenticationRequest) throws PasswordNotAcceptedException {
		return authenticationService.authenticate(authenticationRequest);
	}

	@GetMapping(value = "/refresh", produces = APPLICATION_JSON_VALUE)
	public AuthenticationResponse refresh(HttpServletRequest request) {
		final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (authHeader == null || !authHeader.startsWith("Bearer "))
			throw new MissingAuthHeaderException("Authorization header not presented correctly");

		final String token = authHeader.substring(7); // Trimming 'Bearer '
		return authenticationService.refresh(token);
	}
}
