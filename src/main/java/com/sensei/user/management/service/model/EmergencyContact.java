package com.sensei.user.management.service.model;

import lombok.Data;

@Data
public class EmergencyContact {

	String firstName;
	String lastName;
	String emailAddress;
	String contactNumber;
	String relation;

}
