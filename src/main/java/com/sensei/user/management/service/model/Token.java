package com.sensei.user.management.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document("tokens")
public class Token {

	@Id
	private String id;

	private TokenType type;

	@Indexed(unique = true)
	private String value;

	private String userId;
}