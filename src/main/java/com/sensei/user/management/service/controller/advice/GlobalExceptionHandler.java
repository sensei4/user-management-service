package com.sensei.user.management.service.controller.advice;

import com.sensei.user.management.service.model.exception.IllegalUserUpdateException;
import com.sensei.user.management.service.model.exception.InvalidTokenException;
import com.sensei.user.management.service.model.exception.MissingAuthHeaderException;
import com.sensei.user.management.service.model.exception.PasswordNotAcceptedException;
import com.sensei.user.management.service.model.exception.UserEmailConflictException;
import com.sensei.user.management.service.model.exception.UserNotFoundException;
import lombok.Data;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler({IllegalUserUpdateException.class, IllegalArgumentException.class})
	public ResponseEntity<Error> badRequest(RuntimeException exception) {
		return ResponseEntity.badRequest()
			.contentType(MediaType.APPLICATION_JSON)
			.body(new Error(exception.getMessage()));
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<?> notFound() {
		return new ResponseEntity<>(NOT_FOUND);
	}

	@ExceptionHandler({
		PasswordNotAcceptedException.class,
		MissingAuthHeaderException.class,
		InvalidTokenException.class
	})
	public ResponseEntity<Error> unauthorized(RuntimeException exception) {
		return ResponseEntity.status(UNAUTHORIZED)
			.contentType(MediaType.APPLICATION_JSON)
			.body(new Error(exception.getMessage()));
	}

	@ExceptionHandler({
		UserEmailConflictException.class,
	})
	public ResponseEntity<Error> conflict(RuntimeException exception) {
		return ResponseEntity.status(CONFLICT)
			.contentType(MediaType.APPLICATION_JSON)
			.body(new Error(exception.getMessage()));
	}

	@Data
	static class Error {
		final String error;
	}
}
