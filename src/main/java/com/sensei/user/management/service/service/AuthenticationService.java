package com.sensei.user.management.service.service;

import com.sensei.user.management.service.model.AuthenticationRequest;
import com.sensei.user.management.service.model.AuthenticationResponse;
import com.sensei.user.management.service.model.Token;
import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.model.UserType;
import com.sensei.user.management.service.model.exception.InvalidTokenException;
import com.sensei.user.management.service.model.exception.PasswordNotAcceptedException;
import com.sensei.user.management.service.model.exception.UserEmailConflictException;
import com.sensei.user.management.service.model.exception.UserNotFoundException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationService.class);

	private final BCryptPasswordEncoder passwordEncoder;
	private final UserService userService;
	private final TokenService tokenService;

	public AuthenticationResponse register(User newUser) {
		LOGGER.info("Registering new user: {}", newUser.getEmailAddress());

		if (newUser.getId() != null)
			throw new IllegalArgumentException("Attribute 'id' not permitted");

		try {
			// Will throw UserNotFoundException email address is not registered
			userService.getByEmailAddress(newUser.getEmailAddress());

			throw new UserEmailConflictException("Email address registered to an existing account");
		} catch (UserNotFoundException e) {

			newUser.setCreated(LocalDate.now());
			newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));

			if (userService.adminExists())
				newUser.setUserType(UserType.MEMBER);
			else
				newUser.setUserType(UserType.ADMIN);

			userService.save(newUser);
			LOGGER.info("User saved with ID: {}", newUser.getId());

			return newAuthenticationResponse(newUser);
		}
	}

	public AuthenticationResponse authenticate(AuthenticationRequest request) throws PasswordNotAcceptedException {
		final User storedUser = userService.getByEmailAddress(request.getEmail());

		if (passwordEncoder.matches(request.getPassword(), storedUser.getPassword()))
			return newAuthenticationResponse(storedUser);

		LOGGER.info("Failed login attempt: %s".formatted(request.getEmail()));
		throw new PasswordNotAcceptedException("Invalid credentials");
	}

	public AuthenticationResponse refresh(String tokenValue) {
		if(tokenService.isValidRefreshToken(tokenValue)) {
			final String userId = tokenService.getSubject(tokenValue);
			tokenService.deleteTokensForUserId(userId);
			return newAuthenticationResponse(userService.getById(userId));
		}

		throw new InvalidTokenException("Invalid token presented");

	}

	private AuthenticationResponse newAuthenticationResponse(User user) {
		final Token accessToken = tokenService.generateAccessToken(user);
		final Token refreshToken = tokenService.generateRefreshToken(user);

		return new AuthenticationResponse(accessToken.getValue(), refreshToken.getValue());
	}
}
