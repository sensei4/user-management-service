package com.sensei.user.management.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthenticationResponse {

	@JsonProperty("access")
	private String accessToken;

	@JsonProperty("refresh")
	private String refreshToken;
}
