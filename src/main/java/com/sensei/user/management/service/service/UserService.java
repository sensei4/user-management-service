package com.sensei.user.management.service.service;

import com.sensei.user.management.service.model.User;
import com.sensei.user.management.service.model.UserType;
import com.sensei.user.management.service.model.exception.IllegalUserUpdateException;
import com.sensei.user.management.service.model.exception.UserNotFoundException;
import com.sensei.user.management.service.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.builder.DiffResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

	private final UserRepository userRepository;

	public User save(User user) {
		return userRepository.save(user);
	}

	public List<User> getAll() {
		return userRepository.findAll();
	}

	public User getById(String id) {
		return userRepository.findById(id)
			.orElseThrow(() -> new UserNotFoundException("User not found"));
	}

	public User getByEmailAddress(String emailAddress) {
		return userRepository.findByEmailAddress(emailAddress)
			.orElseThrow(() -> new UserNotFoundException("User not found"));
	}

	public void mergeAndUpdate(String id, User updatedUser) {
		if (isNull(id) || id.isEmpty() || !id.equals(updatedUser.getId()))
			throw new IllegalUserUpdateException("ID must match in path and body");

		User existingUser = getById(id);

		// Setting password so that it's not logged or updated
		updatedUser.setPassword(existingUser.getPassword());
		DiffResult<User> differences = existingUser.diff(updatedUser);
		List<String> diffDetails = differences.getDiffs().stream()
			.map(diff -> "Updating %s from %s to %s".formatted(diff.getFieldName(), diff.getLeft(), diff.getRight()))
			.toList();

		LOGGER.info("Updating user: {}, {}", existingUser.getId(), diffDetails);

		userRepository.save(updatedUser);

		LOGGER.info("User updated: {}", existingUser.getId());
	}

	public void deleteUser(String userId) {
		userRepository.deleteById(userId);
	}

	public boolean adminExists() {
		return count((user) -> user.getUserType() == UserType.ADMIN) > 0;
	}

	private long count(Function<User, Boolean> function) {
		return userRepository.findAll().stream()
			.filter(function::apply)
			.count();
	}

	@Override
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		return getByEmailAddress(username);
	}
}
