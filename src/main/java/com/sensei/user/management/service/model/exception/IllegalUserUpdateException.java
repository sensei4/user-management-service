package com.sensei.user.management.service.model.exception;

public class IllegalUserUpdateException extends RuntimeException{
	public IllegalUserUpdateException(String message) {
		super(message);
	}
}
