package com.sensei.user.management.service.model.exception;

public class UserEmailConflictException extends RuntimeException {
	public UserEmailConflictException(String message) {
		super(message);
	}
}
