package com.sensei.user.management.service.repository;

import com.sensei.user.management.service.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
	Optional<User> findByEmailAddress(String emailAddress);
}
