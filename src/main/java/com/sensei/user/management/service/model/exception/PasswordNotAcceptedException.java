package com.sensei.user.management.service.model.exception;

public class PasswordNotAcceptedException extends RuntimeException {
	public PasswordNotAcceptedException(String message) {
		super(message);
	}
}
