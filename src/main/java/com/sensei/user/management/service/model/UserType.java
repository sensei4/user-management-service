package com.sensei.user.management.service.model;

public enum UserType {
	MEMBER,
	COACH,
	ADMIN
}
