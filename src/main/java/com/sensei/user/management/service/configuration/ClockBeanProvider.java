package com.sensei.user.management.service.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;

@Configuration
public class ClockBeanProvider {

	@Bean
	public Clock clock() {
		return Clock.systemUTC();
	}
}
