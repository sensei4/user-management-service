package com.sensei.user.management.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.builder.DiffResult;
import org.apache.commons.lang3.builder.ReflectionDiffBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Data
@Document("users")
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class User implements UserDetails {
	@Id
	private String id;

	@NonNull
	private String firstName;

	@NonNull
	private String lastName;

	@NonNull
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateOfBirth;

	@NonNull
	@Indexed(unique = true)
	private String emailAddress;

	@NonNull
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String password;

	@NonNull
	private String contactNumber;

	@NonNull
	private UserType userType;

	private List<EmergencyContact> emergencyContactList;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate created;

	@JsonIgnore
	public DiffResult<User> diff(User obj) {
		return new ReflectionDiffBuilder<>(this, obj, ToStringStyle.JSON_STYLE)
			.build();
	}

	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		SimpleGrantedAuthority authority =
			new SimpleGrantedAuthority(userType.name());
		return Collections.singletonList(authority);
	}

	@Override
	@JsonIgnore
	public String getUsername() {
		return emailAddress;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isEnabled() {
		return true;
	}
}


