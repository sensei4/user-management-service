package com.sensei.user.management.service.repository;

import com.sensei.user.management.service.model.Token;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface TokenRepository extends MongoRepository<Token, String> {

	Optional<Token> findByValue(String value);
	void deleteByUserId(String userId);
}
