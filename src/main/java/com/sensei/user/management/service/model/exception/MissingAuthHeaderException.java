package com.sensei.user.management.service.model.exception;

public class MissingAuthHeaderException extends RuntimeException {

	public MissingAuthHeaderException(String message) {
		super(message);
	}
}
