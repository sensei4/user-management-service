package com.sensei.user.management.service.model.exception;

public class InvalidTokenException extends RuntimeException{
	public InvalidTokenException(String message) {
		super(message);
	}
}
